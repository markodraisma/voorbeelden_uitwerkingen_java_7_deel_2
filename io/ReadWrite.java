import java.io.*;

abstract class ReadWrite {
 
  public static Object read(String bestand) throws IOException, ClassNotFoundException {
    try(FileInputStream f = new FileInputStream(bestand);
    BufferedInputStream bis = new BufferedInputStream(f);
    ObjectInputStream in = new ObjectInputStream(bis)){
    Object o = in.readObject();
    
      return o;
    }
  }

  public static void write(Serializable o, String bestand) 
                         throws IOException {
    try(FileOutputStream f = new FileOutputStream(bestand);
        BufferedOutputStream bos = new BufferedOutputStream(f);
        ObjectOutputStream out = new ObjectOutputStream(bos)){
        out.writeObject(o);
    }
    
  }

}
