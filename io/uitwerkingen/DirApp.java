import java.io.*;

class DirApp {

  public static void main(String args[]) throws Exception{

    File file = new File("./a/b/c/d");

    file = file.getAbsoluteFile();
    if(file.mkdirs()){
      System.out.println(file);
    }

    file.delete();
    while((file = file.getParentFile())!=null){
      System.out.println(file);
      file.delete();
    }
    System.out.println(file);
  }

}
