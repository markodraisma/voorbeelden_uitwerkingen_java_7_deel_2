import java.io.*;

class UpperApp {

   public static void main(String args[]) throws IOException{
     
     File input = new File(args[0]);
     if(!input.exists()) 
       throw new IllegalArgumentException("bestand bestaat niet");
     File output = new File(args[1]);
     output.createNewFile();

     try(FileReader fr = new FileReader(input);
         BufferedReader br = new BufferedReader(fr);
         FileWriter fw = new FileWriter(output);
         BufferedWriter bw = new BufferedWriter(fw);
         PrintWriter pw = new PrintWriter(bw)){
         String regel = null;

         while((regel = br.readLine())!=null){
           System.out.println(regel);
           regel=regel.toUpperCase();
           pw.println(regel);
         }
         pw.flush();
     }
   }
}
