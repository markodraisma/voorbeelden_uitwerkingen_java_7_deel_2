import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.*;

class NewDirsApp {

public static void main(String args[]) throws Exception{
  Path newDir = Paths.get("new");
  Path dirs = Paths.get("a/b/c/d");
  newDir = Files.createDirectory(newDir);
  dirs=newDir.resolve(dirs);
  System.out.println("resultaat van resolve: "+ dirs);


  dirs=Files.createDirectories(dirs);
  System.out.println("aangemaakt: " + dirs);
  Files.walkFileTree(newDir, new SimpleFileVisitor<Path>(){

    public FileVisitResult postVisitDirectory(Path dir, IOException ioe) throws IOException {
      System.out.println(dir + " verwijderd: " + Files.deleteIfExists(dir));
      return FileVisitResult.CONTINUE;
    }

  });
}

}
