import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.*;

class test {

   public static void main(String args[]) throws IOException {
     Path p = Paths.get(".");
     Files.walkFileTree(p, new MyVisitor());

   } 

}

class MyVisitor extends SimpleFileVisitor<Path> {

   public FileVisitResult visitFile(Path p, BasicFileAttributes bfa)
                        throws IOException {
     System.out.println("Gevonden bestand: " + p);
     return FileVisitResult.CONTINUE;
  }
  public FileVisitResult preVisitDirectory(Path p, BasicFileAttributes bfa){
     System.out.println("Gevonden directory: " + p);
     if(p.toString().equals(".")){ return FileVisitResult.CONTINUE; }
     return FileVisitResult.SKIP_SUBTREE;
  }

}
