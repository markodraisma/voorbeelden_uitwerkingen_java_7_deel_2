import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

/**
 * Voorbeeld directory watch API
 */
public class ClassWatcher {

  public static void main(String[] args) {
    Path path = Paths.get(".");
    PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**.{class,java}");
    WatchService watchService = null;
    try {
      watchService = path.getFileSystem().newWatchService();
      path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE);
    } catch (IOException e1) {
        e1.printStackTrace();
    }

    for (;;) {
      WatchKey key = null;
      try {
        key = watchService.poll(2, TimeUnit.SECONDS);
      } catch (InterruptedException e1) {
          e1.printStackTrace();
      }
      if (key != null) {
        for (WatchEvent<?> event : key.pollEvents()) {
          Path p = null;
          if(event.context() instanceof Path){
             p = (Path)(event.context());
          }
          switch (event.kind().name()) {
            case "OVERFLOW":
              System.out.println("We lost some events");
              break;
            case "ENTRY_MODIFY":
              if(matcher.matches(p))
              System.out.println("File " + path.resolve(p) + " is modified");
              break;
            case "ENTRY_CREATE":
              if(matcher.matches(p))
              System.out.println("File " + path.resolve(p) + " is created");
              break;
          }
        }
        key.reset();
      }
    }
  }
}
