import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardWatchEventKinds.*;
import java.util.concurrent.TimeUnit;

/**
 * Voorbeeld directory watch API
 */
public class FileWatcher {

  public static void main(String[] args) {
    Path path = Paths.get(".");
    WatchService watchService = null;
    try {
      watchService = path.getFileSystem().newWatchService();
      path.register(watchService, ENTRY_MODIFY, ENTRY_CREATE, 
                    ENTRY_DELETE);
    } catch (IOException e1) {
        e1.printStackTrace();
    }

    for (;;) {
      WatchKey key = null;
      try {
        key = watchService.poll(2, TimeUnit.SECONDS);
      } catch (InterruptedException e1) {
          e1.printStackTrace();
      }
      if (key != null) {
        for (WatchEvent<?> event : key.pollEvents()) {
          switch (event.kind().name()) {
            case "OVERFLOW":
              System.out.println("We lost some events");
              break;
            case "ENTRY_MODIFY":
              System.out.println("File " + event.context() + " is changed");
              break;
            case "ENTRY_CREATE":
              System.out.println("File " + event.context() + " is created");
              break;
            case "ENTRY_DELETE":
              System.out.println("File " + event.context() + " is deleted");
              break;
          }
        }
        key.reset();
      }
    }
  }
}
