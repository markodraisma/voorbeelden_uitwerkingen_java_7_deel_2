import java.awt.FileDialog;
import java.awt.Frame;
import java.io.*;

class Kopie2 {

	public static void main(String args[]) {
		new Kopie2();
	}

	public Kopie2() {
		initDialog();
		System.exit(0);
	}

	public void initDialog() {
		FileDialog fb = new FileDialog(new Frame(), "Kies bronbestand",
				FileDialog.LOAD);
		fb.setVisible(true);
		FileDialog fd = new FileDialog(new Frame(), "Kies doelbestand",
				FileDialog.SAVE);
		fd.setVisible(true);
		File bronBestand = new File(fb.getDirectory(), fb.getFile());
		File doelBestand = new File(fd.getDirectory(), fd.getFile());
		try {
			kopieer(bronBestand, doelBestand);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	public void kopieer(File bron, File doel) throws IOException {
          try(FileInputStream fis = new FileInputStream(bron); 
              BufferedInputStream bis = new BufferedInputStream(fis);
              FileOutputStream fos = new FileOutputStream(doel);
              BufferedOutputStream bos = new BufferedOutputStream(fos)){
              byte[] buffer = new byte[1024];
              int len;
              while((len = bis.read(buffer)) > 0 ){
                 bos.write(buffer,0,len);
                 bos.flush();
              }
          }

	}
}
