import java.io.*;

public class Werknemer implements Serializable
{
  private int persnr;
  private String naam;
  private String functie;
  private int sal;
  private transient Kantoor kantoor;

  private final static long serialVersionUID = 2L;

  public Werknemer()
  {
  }

  public int getPersnr()
  {
    return persnr;
  }

  public void setPersnr(int persnr)
  {
    this.persnr = persnr;
  }

  public String getNaam()
  {
    return naam;
  }

  public void setNaam(String naam)
  {
    this.naam = naam;
  }

  public String getFunctie()
  {
    return functie;
  }

  public void setFunctie(String functie)
  {
    this.functie = functie;
  }

  public int getSal()
  {
    return sal;
  }

  public void setSal(int sal)
  {
    this.sal = sal;
  }

  public Kantoor getKantoor()
  {
    return kantoor;
  }

  public void setKantoor(Kantoor kantoor)
  {
    this.kantoor = kantoor;
  }
 
  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    //in.defaultReadObject();
    this.persnr = in.readInt();
    setNaam(in.readUTF());
    setFunctie(in.readUTF());
    setSal(in.readInt());
    kantoor = new Kantoor();
    kantoor.setKantnr(in.readInt());
    kantoor.setNaam(in.readUTF());
    kantoor.setPlaats(in.readUTF());
  }

  private void writeObject(ObjectOutputStream out) throws IOException {
    //out.defaultWriteObject();
    out.writeInt(this.persnr);
    out.writeUTF(this.naam);
    out.writeUTF(this.functie);
    out.writeInt(this.sal);
    out.writeInt(kantoor.getKantnr());
    out.writeUTF(kantoor.getNaam());
    out.writeUTF(kantoor.getPlaats());
  }

}
