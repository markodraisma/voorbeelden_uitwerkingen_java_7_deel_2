import java.io.*;
import java.util.Collection;
import java.util.ArrayList;
import java.nio.file.*;

public class WerknemersKantoren 
{   
  // een dubbele backslash om niet aan te geven dat er een escape karakter gebruikt moet worden
  static String lokatie = ".\\";
  static File f = new File(lokatie+"werknemers_kantoren.txt");

  public static void main(String[] args)
  {
    maakObjecten();
    leesObjecten();
    watch();
  }
    


    public static void watch(){
    Path p = Paths.get(lokatie);
    Path p2 = f.toPath();
    WatchService ws = null;
    try{
      ws = p.getFileSystem().newWatchService();
      p.register(ws, StandardWatchEventKinds.ENTRY_MODIFY);
    }catch(IOException ioe){ioe.printStackTrace();}
    for(;;){
      try{
         WatchKey key = ws.take();
         for(WatchEvent<?> event: key.pollEvents()){
           Path path = (Path)(event.context());
            switch(event.kind().name()){
               case "OVERFLOW":
                 System.out.println("Overflow event occurred");
                 break;
               case "ENTRY_MODIFY":
                 if(Files.exists(path) && Files.isSameFile(path,p2)){
                   maakObjecten();
                   leesObjecten();
                 }
            }
            key.reset();
         }
      }
      catch(InterruptedException|IOException ie){
        ie.printStackTrace();
      }
    }
    }

  
  static void maakObjecten()
  {
    try(FileReader in = new FileReader(f);
      LineNumberReader lineIn = new LineNumberReader(in))
    {
    //  FileReader in = new FileReader(f);
    //  LineNumberReader lineIn = new LineNumberReader(in);
      boolean end = false;
      while (!end)
      {
        try
        {
          Werknemer werkn = new Werknemer();
          // eigenschappen werknemer inlezen
          int persnr = Integer.parseInt(lineIn.readLine());
          werkn.setPersnr(persnr);
          werkn.setNaam(lineIn.readLine());
          werkn.setFunctie(lineIn.readLine());
          werkn.setSal(Integer.parseInt(lineIn.readLine()));

          Kantoor kant = new Kantoor();
          kant.setKantnr(Integer.parseInt(lineIn.readLine()));
          kant.setNaam(lineIn.readLine());
          kant.setPlaats(lineIn.readLine());
          // eigenschappen kantoor inlezen en kantoor aan werknemer koppelen
          werkn.setKantoor(kant);
          // de achtste regel is de scheidingsregel naar de volgende werknemer
          // als deze niet bestaat is het einde van de file bereikt.
          if (lineIn.readLine() == null)
          {
            end = true;
          }

          // Schrijf de werknemer weg als object:
          try(FileOutputStream fos = new FileOutputStream(lokatie+persnr+".obj");
          ObjectOutputStream out = new ObjectOutputStream(fos)){
            out.writeObject(werkn);
          }

        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      }
    }
    catch (IOException e)
    {
      e.printStackTrace(); 
    }
  }
  
  static void leesObjecten()
  {
    Collection<Werknemer> werknemers = new ArrayList<Werknemer>();
    File dir = new File(lokatie);
    String[] listing = dir.list();
    for (int i=0; i<listing.length; i++)
    {
      if (listing[i].endsWith(".obj"))
      {
        try
        {
          FileInputStream fis = new FileInputStream(listing[i]);
          ObjectInputStream ois = new ObjectInputStream(fis);
          Werknemer werkn = (Werknemer)ois.readObject();
          werknemers.add(werkn);
          // lees werknemer in en voeg toe aan vector werknemers
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    }
    if (werknemers != null && werknemers.size()>0)
    {
      File f = new File(lokatie+"werknemers_tabel.csv");
      try(PrintWriter writer = new PrintWriter(f)){
         // doorloop de werknemers vector
         // en schrijf elke werknemer als regel weg in werknemers_tabel.csv
         for(Werknemer werkn: werknemers){
            Kantoor kant = werkn.getKantoor();
            writer.printf("%d,%s,%s,%d,%d,%s,%s%n",
                           werkn.getPersnr(),
                           werkn.getNaam(),
                           werkn.getFunctie(),
                           werkn.getSal(),
                           kant.getKantnr(),
                           kant.getNaam(),
                           kant.getPlaats());
         }

      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }
    
    
  }
}
