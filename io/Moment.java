import java.io.*;
import java.util.*;

class Test implements Serializable 
{
   Test(String naam){

   }
   //Test(){}
}

class Moment extends Test implements Serializable {
  
  private final static long serialVersionUID = 2;

  Moment(){
     super("moment");
  }

  private Date tijd=null; 
  
  public void setTijd() {
    Calendar c = Calendar.getInstance();
    tijd = c.getTime();
  }
  
  public Date getTijd() {
    return tijd;
  }

}
