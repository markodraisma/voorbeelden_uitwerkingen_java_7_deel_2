
class ResourceApp {
  public static void main(String args[]) throws Exception {
     Resource r2=null;
     try( Resource r = new Resource()){
        r2=r;
        r.doWork();
     }
     /*finally {
       System.out.println("explicit finally");
       try {
       r2.close();
       } catch(Exception e){
         System.err.println(
           "Exception from explicit finally: " +e);
       }
     } */
  }

}


