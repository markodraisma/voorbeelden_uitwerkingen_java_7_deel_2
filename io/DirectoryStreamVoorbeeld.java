import java.nio.file.*;
import java.io.*;

class DirectoryStreamVoorbeeld {
  public static void main(String args[]) throws IOException {
    try( DirectoryStream<Path> stream = 
      Files.newDirectoryStream(Paths.get("."),"*.{class,java}")){
     for(Path path:stream){
       System.out.println(path);
     }
    }
    System.out.println();
    System.out.println("met filter: ");
    try( DirectoryStream<Path> stream = 
      Files.newDirectoryStream(Paths.get("."),new Filter())){
     for(Path path:stream){
       System.out.println(path);
     }
    }
  }

  static class Filter implements DirectoryStream.Filter<Path> {
    public boolean accept(Path p){
      try {
      return Files.size(p)>1024;
      } catch(IOException ioe){
        throw new RuntimeException(ioe);
      }
    }
  }
}
