import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TraditionalFile {

  public static void main(String[] args) {
    try {
      readFile();
    } catch (IOException e) {
        e.printStackTrace();
        System.out.println("suppressed: ");
	Throwable[] throwables = e.getSuppressed();
	for (Throwable throwable : throwables)
          for(StackTraceElement element: throwable.getStackTrace())
             System.err.println(element);
    }
  }

  public static void readFile() throws IOException {
    BufferedReader bufferedReader = null;
    Exception suppressedException = null;
    try {
      File file = new File("");
      FileReader fileReader = new FileReader(file);
      bufferedReader = new BufferedReader(fileReader);
      String lineRead = null;
      while ((lineRead = bufferedReader.readLine()) != null) {
        System.out.println(lineRead);
      }
    } catch (IOException e) {
        suppressedException = e;
        System.out.println("IOException in de catch");
	throw e;
    } finally {
        try {
          bufferedReader.close();
        } catch (Exception e) {
	    if (suppressedException != null) {
              e.addSuppressed(suppressedException);
            }
            System.out.println("IOException in finally");
            throw e;
        }
    }
  }
}
