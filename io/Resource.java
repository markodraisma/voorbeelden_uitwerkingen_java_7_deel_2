public class Resource implements AutoCloseable {
  
  private int teller;
  
  public void doWork() throws Exception {
    System.out.println("work " + teller++);
  //  throw new Exception("Exception thrown from doWork()");
  }

  @Override
  public void close() throws Exception {
    System.out.println("close " + teller++);
    throw new Exception("Exception thrown from close()");
  }

}
