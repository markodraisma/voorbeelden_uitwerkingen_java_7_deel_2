import java.sql.*;
import java.util.*;
import javax.sql.*;
import javax.sql.rowset.*;

public class JdbcKlantDAO implements KlantDAO{

   private JdbcRowSet rowSet;


   JdbcKlantDAO(){
     try{RowSetFactory factory = RowSetProvider.newFactory();
     rowSet = factory.createJdbcRowSet();
     final String username = "cursist", password = "vh_cursus", url="jdbc:mysql://localhost:3306/javacursus";
     rowSet.setUsername(username);
     rowSet.setPassword(password);
     rowSet.setUrl(url);
     // tabel aanmaken indien die nog niet bestaat
     try(Connection conn = DriverManager.getConnection(url,username,password);
     Statement stmt = conn.createStatement()){
       stmt.executeUpdate("create table if not exists klanten  ("+
       "id INTEGER NOT NULL default 0 primary key " +
       ", NAAM VARCHAR(20))");
     }
     rowSet.setAutoCommit(false);
     }catch(SQLException sqle){sqle.printStackTrace();}
   }
   
   public Klant findById(int id){
     Klant k = null;
     try{
       rowSet.setCommand("select id, naam from klanten where id = ?");
       rowSet.setInt(1,id);
       rowSet.execute();
       if(rowSet.next()){
         k = new Klant();
         k.setId(rowSet.getInt(1));
         k.setNaam(rowSet.getString(2));
         System.out.println("klant opgehaald");
       }
     }catch(SQLException sqle){sqle.printStackTrace();}
     return k;
   }

   public Collection<Klant> findAll(){
     Collection<Klant> klanten = new ArrayList<>();
     try{
       rowSet.setCommand("select id, naam from klanten");
       rowSet.execute();
       while(rowSet.next()){
         Klant k = new Klant(rowSet.getInt(1), rowSet.getString(2));
         klanten.add(k);
       }
     }catch(SQLException sqle){
       sqle.printStackTrace();
     }
     return klanten;
   }


   public void create(Klant k){
     // als id al bestaat: wijzigen in plaats van toevoegen
     Klant k1 = findById(k.getId());
     if(k1!= null){
       update(k);
     }
     else
       try{
         rowSet.setCommand("select id, naam from klanten");
         rowSet.execute();
         rowSet.moveToInsertRow();
         rowSet.updateInt(1,k.getId());
         rowSet.updateString(2,k.getNaam());
         rowSet.insertRow();
         System.out.println("klant toegevoegd");
         rowSet.commit();
       }catch(SQLException sqle){sqle.printStackTrace();}
   }

   public void update(Klant k){
     try{
       rowSet.setCommand("select id, naam from klanten where id = ?");
       rowSet.setInt(1,k.getId());
       rowSet.execute();
       if(rowSet.next()){
         rowSet.updateInt(1,k.getId());
         rowSet.updateString(2,k.getNaam());
         rowSet.updateRow();
         rowSet.commit();
         System.out.println("klant gewijzigd");
       }
     }catch(SQLException sqle){sqle.printStackTrace();}
   }
 
   public void delete(Klant k) {
     try{
       rowSet.setCommand("select id, naam from klanten where id = ?");
       rowSet.setInt(1,k.getId());
       rowSet.execute();
       if(rowSet.next()){
         rowSet.deleteRow();
         rowSet.commit();
       }
       System.out.println("klant verwijderd");
     }catch(SQLException sqle){sqle.printStackTrace();}
   }
}
