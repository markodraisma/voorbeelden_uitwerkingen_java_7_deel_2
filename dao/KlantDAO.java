import java.util.Collection;

public interface KlantDAO{

   public Klant findById(int id);
   public Collection<Klant> findAll();
   public void create(Klant k);
   public void update(Klant k);
   public void delete(Klant k);

}
