import java.util.*;
import java.nio.file.*;
import java.io.*;

public class FileKlantDAO implements KlantDAO {

   private Path path = Paths.get("klanten.csv");

   private Comparator<Klant> comparator = new Comparator<Klant>(){
     public int compare(Klant k1, Klant k2){
       return k1.getId()-k2.getId();
     }
   };

   private Collection<Klant> klanten = new TreeSet<>(comparator);

   FileKlantDAO(){
     try{
      if(!Files.exists(path))Files.createFile(path);
     }catch(Exception e){System.err.println(e);}
   }

   public Collection<Klant> findAll(){
     File file = path.toFile();
     klanten.clear();
      try(      Scanner scanner = new Scanner(file)){
      while(scanner.hasNextLine()){
        Scanner rowScanner = new Scanner(scanner.nextLine());
        rowScanner.useDelimiter(";");
        Klant k = new Klant();
        k.setId(rowScanner.nextInt());
        k.setNaam(rowScanner.next());
        klanten.add(k);
      }
      } catch(IOException ioe){
        ioe.printStackTrace();
      }
      return klanten;
   }

   public Klant findById(int id){
     klanten = findAll();
     for(Klant k: klanten){
       if(id==k.getId())
         System.out.println("klant gevonden");
         return k;
     }
     return null;
   }

   public void create(Klant k){
      klanten = findAll();
      klanten.add(k);
      try(
      FileWriter fw = new FileWriter(path.toFile());
      BufferedWriter bw = new BufferedWriter(fw);
      PrintWriter pw = new PrintWriter(bw)){
      for(Klant klant:klanten){
        pw.printf("%d;%s%n",klant.getId(), klant.getNaam());
      }
      pw.flush();
      System.out.println("klant aangemaakt");
      }catch(IOException ioe){ioe.printStackTrace();};
   }

   public void delete(Klant k){
      klanten = findAll();
      try(
      FileWriter fw = new FileWriter(path.toFile());
      BufferedWriter bw = new BufferedWriter(fw);
      PrintWriter pw = new PrintWriter(bw)){
      for(Klant klant:klanten){
        if(klant.getId()!=k.getId())
        pw.printf("%d;%s%n",klant.getId(), klant.getNaam());
      }
      pw.flush();
      System.out.println("klant verwijderd");
      }catch(IOException ioe){ioe.printStackTrace();};

   }

   public void update(Klant k){
      klanten = findAll();
      try(
      FileWriter fw = new FileWriter(path.toFile());
      BufferedWriter bw = new BufferedWriter(fw);
      PrintWriter pw = new PrintWriter(bw)){
      for(Klant klant:klanten){
        if(klant.getId()!=k.getId())
        pw.printf("%d;%s%n",klant.getId(), klant.getNaam());
        else
        pw.printf("%d;%s%n",k.getId(), k.getNaam());
      }
      pw.flush();
      System.out.println("klant gewijzigd");
      }catch(IOException ioe){ioe.printStackTrace();};
   }

}
