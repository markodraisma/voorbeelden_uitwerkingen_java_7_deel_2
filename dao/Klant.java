public class Klant {
  private int id;
  private String naam;

  public Klant(int id, String naam){
    this.id=id;
    this.naam=naam;
  }

  public Klant(){}

  public String getNaam(){
    return naam;
  } 

  public int getId(){
    return id;
  }

  public void setId(int id){
    this.id=id;
  }

  public void setNaam(String naam){
    this.naam=naam;
  }

  public String toString(){
     return new StringBuilder().append(id).append(": ").append(naam).toString();
  } 

  public boolean equals(Object o){
     return o instanceof Klant && ((Klant)o).getId()==this.id;
  }

}
