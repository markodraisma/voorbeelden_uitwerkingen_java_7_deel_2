public abstract class KlantDAOFactory {

   public abstract KlantDAO getDao();

   public static KlantDAOFactory getInstance(String type){
     switch(type){
       case "FILE": return new KlantDAOFactory(){
          public KlantDAO getDao(){
             return new FileKlantDAO();
          }
       };
       case "DB": return new KlantDAOFactory(){
          public KlantDAO getDao(){
             return new JdbcKlantDAO();
          }
       };
       default: throw new IllegalArgumentException("DAO type not supported");
     }
   }
}
