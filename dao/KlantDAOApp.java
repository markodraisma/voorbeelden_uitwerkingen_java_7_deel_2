public class KlantDAOApp{

  public static void main(String args[]){
     KlantDAOFactory factory = KlantDAOFactory.getInstance("FILE");
     KlantDAO dao = factory.getDao();

     dao.create(new Klant(1, "piet"));
     dao.create(new Klant(2, "klaas"));
     dao.create(new Klant(3, "gerrit"));
     for(Klant k: dao.findAll()){
       System.out.println(k);
     }
     dao.delete(new Klant(1, "piet"));
     dao.update(new Klant(2, "harry"));
     for(Klant k: dao.findAll()){
       System.out.println(k);
     }
     System.out.println(dao.findById(2));

  }

}
