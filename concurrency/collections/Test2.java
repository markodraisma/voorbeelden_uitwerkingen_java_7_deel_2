import java.util.*;
import java.util.concurrent.*;

class Test2 {
  
   static List<Integer> lijst = new CopyOnWriteArrayList<>();

   public static void main(String args[]){

     Collections.addAll(lijst,1,2,3,4);
     for(int i:lijst){
       System.out.println(i);
       lijst.add(i);
     }
     System.out.println();
     for(int i:lijst){
       System.out.println(i);
     }

   }

}
