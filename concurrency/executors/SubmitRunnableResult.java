import java.util.concurrent.*;

class SubmitRunnableResult {

   final static StringBuilder sb = new StringBuilder();
   
   public static void main(String args[]){
      ExecutorService es = Executors.newSingleThreadExecutor();
      Future<StringBuilder> future = es.submit(new StringTaak(), sb);
      try{
        System.out.println(future.get().toString());
        es.shutdown();
      }catch(InterruptedException | ExecutionException e){}

   }

   static class StringTaak implements Runnable {
     
      public void run(){
        sb.append("test");
      }
   }
}
