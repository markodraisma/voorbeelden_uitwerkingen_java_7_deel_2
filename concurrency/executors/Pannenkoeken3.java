import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.*;

class Pannenkoek {}

class Pannenkoeken {

  private final Deque<Pannenkoek> stapel = new LinkedList<>();
  private final int maxAantal;

  Pannenkoeken(int maxAantal){
    this.maxAantal = maxAantal;
  }

  public void bak(){
    stapel.addFirst(new Pannenkoek());
  }
  
  public Pannenkoek pak() {
    return stapel.removeFirst();
  }

  public boolean op(){
    return stapel.size()==0;
  }

  public boolean klaar(){
    return stapel.size()==maxAantal;
  } 

}


class Feestje {
  final static Lock lock = new ReentrantLock(true);
  final static Condition empty = lock.newCondition();
  final static Condition yield = lock.newCondition();
  final static Condition full = lock.newCondition();
  final static Pannenkoeken pannenkoeken = new Pannenkoeken(10);
  static boolean doorgaan=true;
  public static void main(String args[]) throws InterruptedException {
    ExecutorService executor = Executors.newFixedThreadPool(4);
    Bakker bakker = new Bakker();
    Kind henk = new Kind("Henk");
    Kind piet = new Kind("Piet");
    Kind iris = new Kind("Iris");

    executor.execute(bakker);
    executor.execute(henk);
    executor.execute(piet);
    executor.execute(iris);
    executor.shutdown();
    while(!executor.awaitTermination(10, TimeUnit.SECONDS)){
       System.out.println("nog bezig");
    }
    System.out.println("Feestje is afgelopen");

  }
 
  static class Bakker extends Thread {

    public void run(){
      // 5 ladingen bakken
      for(int i=0;i<5;i++){
        bakken();
      }
      doorgaan=false;
    }

    public void bakken(){
      try {
        lock.lock();
        while(!pannenkoeken.op()){
          try{
            empty.await();
          }
          catch(InterruptedException ie){
            System.err.println(ie);
          }
        }
        while(!pannenkoeken.klaar()){
          pannenkoeken.bak();
          try{Thread.sleep(50 + (long)(Math.random()*100));}
          catch(InterruptedException ie){}
        }
        System.out.println("Pannenkoeken zijn klaar!");
        full.signalAll();
      }
      finally{
        lock.unlock();
      }
    }
  }

  static class Kind extends Thread {

    private final String naam;

    Kind( String naam){
      this.naam=naam;
    }

    public void run(){
      while(doorgaan){
        eten();
      }
    }

    public void eten(){
      int count=0;
      try{ lock.lock();
        while(pannenkoeken.op()){
          try{full.await(1, TimeUnit.SECONDS);}
          catch(InterruptedException ie){}
        }
        while(!pannenkoeken.op()&& count++<=3){
          pannenkoeken.pak();
          System.out.println("pannenkoek gegeten door " + naam);
        }
        if(pannenkoeken.op())
        empty.signal();
      }
      finally{
        lock.unlock();
      }
      
    }
  }
}

