import java.util.Calendar;
import java.util.Arrays;
import java.util.concurrent.*;

class Klok extends Thread {

  char[] wis;
  
  Klok() {
    wis = new char[29];     // character array van 30 elementen lang
    Arrays.fill(wis,'\b');  // wis vullen met backspace characters
  }

  public static void main(String args[]) throws Exception {
    Klok klok = new Klok();
    ScheduledExecutorService ses = Executors.newScheduledThreadPool(10);
    ScheduledFuture<?> sf = ses.scheduleAtFixedRate(klok,0,1,TimeUnit.SECONDS);
    ses.awaitTermination(60,TimeUnit.SECONDS);
    ses.shutdown();
  }

  public void run() {
      Calendar c= Calendar.getInstance();  // huidige moment ophalen
      System.out.print(wis);               // eerst regel leeg maken
      System.out.print(c.getTime());       // dan tijd uitprinten
      System.out.flush();                  // en weergeven
  }
}
