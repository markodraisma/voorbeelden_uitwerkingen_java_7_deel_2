import java.io.IOException;
import java.nio.file.*;

public class PathInfo {
  public static void main(String[] args) throws IOException {
    Path path = Paths.get("test.txt");
    System.out.println("Path info:");
    System.out.println("file name: " + path.getFileName());
    System.out.println("parent: " + path.getParent());
    System.out.println("root: " + path.getRoot());
    System.out.println("URI: " + path.toUri());
    System.out.println("normalized: " + path.normalize());
    System.out.println("absolute: " + path.toAbsolutePath());
    System.out.println("resolve: " + path.resolve(Paths.get("C:/docs/test")));
    System.out.println("relativize: " + path.relativize(Paths.get("C:/Temp")));
    System.out.println("realpath: " +
      path.toRealPath(LinkOption.NOFOLLOW_LINKS));
    System.out.println("subpath (1, 2): " + path.subpath(1,2));
  }
}

