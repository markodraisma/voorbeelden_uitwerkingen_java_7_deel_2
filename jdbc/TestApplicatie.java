import java.sql.*;

public class TestApplicatie {

  public static void main (String args []) {
    try(Connection con = MijnConnectie.connect()){
      DatabaseMetaData dmd = con.getMetaData();
      System.out.println("Database: "  + '\n' + dmd.getDatabaseProductVersion());
      System.out.println("Maximaal aantal kolommen in een tabel: " + 
      dmd.getMaxColumnsInTable());
      System.out.println("Gebruikersnaam: " + dmd.getUserName());
    } catch(SQLException sqle){
      System.err.println(sqle);
    }
  }

}
