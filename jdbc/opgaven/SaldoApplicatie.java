import java.sql.*;


public class SaldoApplicatie 
{
  
  public static void main(String[] args) throws SQLException
  {
    Connection conn = MijnConnectie.connect();
    Statement stMut = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); // pas deze regel aan
    
  ResultSet rsMut = stMut.executeQuery
		  ("select id, klantnr, mutatie, uitgevoerd from j_saldomutatie for update");
    
	  PreparedStatement psKlan = conn.prepareStatement
		  ("update j_klanten set opensaldo = opensaldo + ? where klantnr = ?");

   while(rsMut.next()){
      psKlan.setInt(1,rsMut.getInt(3));
      psKlan.setInt(2,rsMut.getInt(2));
      psKlan.executeUpdate();
      rsMut.updateInt(4,1);
      rsMut.updateRow();
   }
    
    conn.commit();

  

  rsMut.close();
  psKlan.close();
  stMut.close();
  conn.close();
  }
}
