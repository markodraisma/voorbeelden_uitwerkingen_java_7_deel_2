import java.sql.*;
import javax.sql.*;
import javax.sql.rowset.*;

public class SaldoApplicatie2
{

  public static void main(String[] args) throws SQLException
  {
        RowSetFactory factory = RowSetProvider.newFactory();
    try(Connection conn = MijnConnectie.connect();
        JdbcRowSet rsMut = factory.createJdbcRowSet();
        PreparedStatement psKlan = conn.prepareStatement("update j_klanten set opensaldo = opensaldo - ? where klantnr = ?")){

        rsMut.setUrl("jdbc:mysql://localhost:3306/javacursus");
        rsMut.setUsername("cursist");
        rsMut.setPassword("vh_cursus");
      rsMut.setCommand
        ("select id, klantnr, mutatie, uitgevoerd from j_saldomutatie for update");
        rsMut.setAutoCommit(false);
      rsMut.execute();

      while(rsMut.next()){
        psKlan.setInt(1,rsMut.getInt(3));
        psKlan.setInt(2,rsMut.getInt(2));
        psKlan.executeUpdate();
        rsMut.updateInt(4,0);
        rsMut.updateRow();
      }
      rsMut.commit();
      conn.commit();
    }
  }
}
