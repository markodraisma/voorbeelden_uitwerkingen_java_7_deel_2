import java.sql.*;

public class InsertKlant {
  public static void main(String args[]){
     //Connection conn=MijnConnectie.connect();
     try(Connection conn=MijnConnectie.connect();
Statement stmt = conn.createStatement()){
        int rows = stmt.executeUpdate("insert into j_klanten values " +
        "(1012, 'de Koning', 'Gierstraat', '88', '9823HD', 'Nieuwegein', 760)");
        System.out.println("Aantal verwerkte rijen: " + rows);
        conn.commit();

     } catch(SQLException sqle){
     //  System.err.println(sqle);
       sqle.printStackTrace();
     }

  }
}
