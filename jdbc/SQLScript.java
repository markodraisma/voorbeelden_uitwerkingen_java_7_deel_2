import java.io.*;
import java.sql.*;
import java.util.*;

class SQLScript {

  public static void main(String args[]) {
    try(Connection conn=MijnConnectie.connect();
    Statement stm = conn.createStatement()){
    for(String regel:openFile(args[0])) {
      stm.addBatch(regel);
    }
    int[] regels = stm.executeBatch();
    for(int i:regels){
      System.out.println(i+" regels verwerkt.");
    }
    conn.commit();
    } catch(SQLException sqle){
      System.err.println(sqle);
    }
  }
  
  private static Iterable<String> openFile(String naam) {
    List<String> regels=null; 
    File bestand = new File(naam);
    try(Scanner scanner = new Scanner(bestand)){
      regels = new ArrayList<>();
      String tekst;
      while(scanner.hasNextLine()){
        tekst=scanner.nextLine();
        regels.add(tekst.replace(';',' ').trim());
      }
    }
    catch(IOException ioe){System.err.println(ioe);}
    return regels;
  }

}
