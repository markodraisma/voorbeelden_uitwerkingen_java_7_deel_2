import java.sql.*;

public class MijnConnectie {

  private final static String url = "jdbc:mysql://localhost:3306/javacursus";

  public static Connection connect(){
    try{Connection conn=DriverManager.getConnection(url,"cursist","vh_cursus");
        conn.setAutoCommit(false);
        return conn;
    }
    catch(SQLException sqle){
      System.err.println("Fout bij het inloggen: " + sqle.getMessage());
      return null;
    }

  }

}
