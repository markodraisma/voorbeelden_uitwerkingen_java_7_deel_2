import java.sql.*;

class PreparedUpdate {

  public static void main (String args []) {

    try(Connection conn = MijnConnectie.connect();
        PreparedStatement pstmt =
        conn.prepareStatement ("update j_klanten set opensaldo = ? where naam = ?")){

      pstmt.setInt(1, 2500);
      pstmt.setString(2, "Willemsen");
      System.out.println(pstmt.executeUpdate ()+ " rijen verwerkt");

      pstmt.setInt(1, 0);
      pstmt.setString(2, "de Koning");
      System.out.println(pstmt.executeUpdate ()+ " rijen verwerkt");

      conn.commit();
    }
    catch(SQLException sqle){
      System.err.println(sqle);
    }
  }
}

