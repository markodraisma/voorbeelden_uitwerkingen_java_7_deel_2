import java.util.*;
public class ThreadVoorbeeld {

    public static void main(String[] args) {
        List<Thread> list = new ArrayList<>();

        for (int i = 0; i < 8; i++) {
            Taak taak = new Taak(i + 1);
            Thread t = new Thread(taak);
            list.add(t);
            t.start();
            t.setDaemon(true);
            if(i==5)t.interrupt();
        }
    /*    for(Thread t: list){
            try {
            t.join();
            } catch(InterruptedException ie){
              System.out.println(ie);
            }
        }*/
        System.out.println("Main methode afgerond...");
    }
}

class Taak implements Runnable {

    private int num;

    public Taak(int num) {
      this.num=num;
    }


    public void run() {
        System.out.println("Uitvoeren van taak " + num + "...");
        try {
            Thread.sleep(0);
        } catch (InterruptedException e) {
          System.err.println(e.getMessage());} 
        System.out.println("taak " + num + " afgerond...");
    }
}
