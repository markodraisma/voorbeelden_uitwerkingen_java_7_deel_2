class ThreadState extends Thread {

  public static void main(String args[]){
    Thread t = new ThreadState();
    System.out.println("main: " +t.getState());
    t.start();
    while(true){
      System.out.println("main: " + t.getState());
      try{
        Thread.sleep(700);
        if(t.getState()==Thread.State.TERMINATED) break;
      }catch(InterruptedException ie){}
    }
    System.out.println(t.getState());
  }

  public void run(){
    for(int i=1;i<3;i++){
      try{Thread.sleep(500);
      } catch(InterruptedException ie){}
    System.out.println("run: " + this.getState());
    }
  }
}
