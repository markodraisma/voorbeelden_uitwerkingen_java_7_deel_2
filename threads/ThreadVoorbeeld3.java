import java.util.*;
public class ThreadVoorbeeld3 {

    public static void main(String[] args) {
        List<Taak> taken = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            Taak taak = new Taak(i + 1);
            taken.add(taak);
            taak.start();
        }
        for(Taak taak: taken){
         try{
            taak.join();
          } catch(InterruptedException ie){
              ie.printStackTrace();
          }
        }
        System.out.println("Main methode afgerond...");
    }
}

class Taak extends Thread {

    private int num;

    public Taak(int num) {
      this.num=num;
    }


    public void run() {
        System.out.println("Uitvoeren van thread " + num + "...");
        try {
            Thread.sleep((long)(500 + Math.random() * 500));
        } catch (InterruptedException e) {}
        System.out.println("thread " + num + " afgerond...");
    }
}
