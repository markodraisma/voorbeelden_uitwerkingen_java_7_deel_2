import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

class TellerVoorbeeld {

  private static AtomicInteger teller = new AtomicInteger();


  public static void main(String args[]){
    List<Thread> threads = new ArrayList<>();
    for(int i=0;i<1000;i++){
      threads.add(new Minus());
      threads.add(new Plus());
    }
    for(Thread t:threads){
      t.start();
    }
    for(Thread t:threads){
      try{
      t.join();}
      catch(InterruptedException ie){ie.printStackTrace();}
    }
    System.out.println("Na afloop is de waarde van teller: " + teller);
  }

  static int plus(){
       return teller.incrementAndGet();
  }

  static int min(){
      return teller.decrementAndGet();
  }

  static class Minus extends Thread {
     public void run(){
       System.out.println(min());
     }
  }

  static class Plus extends Thread {
    public void run(){
      System.out.println(plus());
    }
  }
}
