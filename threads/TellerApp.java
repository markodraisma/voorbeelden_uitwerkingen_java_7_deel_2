public class TellerApp {

     public static void main(String[] args) {
    
      for (int i=0;i<90;i++){
        Teller teller = new Teller(i);
        Thread thread = new Thread(teller);
        if(i<30) thread.setPriority(Thread.MIN_PRIORITY);
        else if(i<60) thread.setPriority(Thread.NORM_PRIORITY);
        else if(i<90) thread.setPriority(Thread.MAX_PRIORITY);
        thread.start();
      }
    }
}

class Teller implements Runnable {

    private final int id;

    Teller(int id){
      this.id=id;
    }

    public void run() {
       long starttijd = System.currentTimeMillis();
       long teller=0;
       while (teller < 500000000L){
         teller++;
         if(teller%10==0)
         Thread.yield();
       }
       long tijd = System.currentTimeMillis() - starttijd;
       System.out.println(id + ": "+ tijd);
     }
}
