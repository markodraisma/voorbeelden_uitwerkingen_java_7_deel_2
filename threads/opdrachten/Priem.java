import java.util.concurrent.*;
import java.util.*;

class Priem extends RecursiveAction {

   private final static List<Long> priems = new CopyOnWriteArrayList<>();
   private final static ForkJoinPool fjp = new ForkJoinPool();
   private static int maxThreadCount = 0;

   public static void main(String args[]) throws Exception{
      Priem priem = new Priem(Long.parseLong(args[0]),Long.parseLong(args[1]));
      fjp.invoke(priem);
      Long[] values = priems.toArray(new Long[0]);
      Arrays.sort(values);
      for(long p: values){
        System.out.println(p);
      }
      System.out.println("Maximaal aantal threads: "+maxThreadCount);
   }


   private final long start, einde, THRESHOLD=100;

   Priem(long start, long einde){
     this.start=start;
     this.einde=einde;
   }

   public void compute(){
      int threadCount = fjp.getActiveThreadCount();
      maxThreadCount = threadCount>maxThreadCount?threadCount:maxThreadCount;
      if(einde-start<=THRESHOLD){
        toon_priems(start,einde);
      }
      else{
         Priem p1= new Priem(start, start + (einde-start)/2);
         Priem p2= new Priem(start + (einde - start)/2, einde);
         invokeAll(p1,p2);
      }
   }

   public void toon_priems(long a, long b){
     for(long i=a;i<b;i++){
        if(isPriem(i)){
          priems.add(i);
        }
     }
   }

   private boolean isPriem(long getal){
      if(getal==1) return false;
      if(getal==2) return true;
      if(getal%2==0) return false;
      for(long deler=3;deler<=Math.sqrt(getal);deler+=2){
         if(getal%deler==0) return false;
      }
      return true;
   }

}
