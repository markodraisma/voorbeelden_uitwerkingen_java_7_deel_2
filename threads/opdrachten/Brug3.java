import java.awt.Color;
import java.awt.Graphics;
import java.util.concurrent.atomic.AtomicBoolean;

public class Brug3 implements Runnable {
  private final AtomicBoolean open = new AtomicBoolean(false);                       
  int positie, lengte;
  private Thread thread;
  
  
  public Brug3(int positie, int lengte) {  // maak brug aan
    this.positie=positie;                 // met bepaalde positie en lengte
    this.lengte=lengte;
    thread= new Thread(this);
    thread.start();
  } 
  
  public void run(){
    while(true){
      openSluit();
      try{Thread.sleep(open()?500:1000);} // laat de brug langer gesloten dan open
      catch(InterruptedException ie){}
    }
  }
  
  public void openSluit() {              // open of sluit de brug
                                         // als de brug open is, zet 'm dan dicht
                                         // anders: alleen openen als de brug niet bezet is
      open.compareAndSet(open.get()?true:Auto3.brugBezet(),!open.get()); 
  }

  public boolean open() {
    return open.get();
  }
  
  public void paint(Graphics g) {        // teken een geopende of gesloten brug
    g.setColor(Color.red);
    if(open())
      g.drawLine(positie,100,positie,100-lengte);
    else                            
      g.drawLine(positie,100,positie+lengte,100);
  }
  
}
