
import java.util.concurrent.*;
import java.math.BigInteger;

class Faculteit extends RecursiveTask<BigInteger> {


   final private BigInteger start, einde;
   private static BigInteger THRESHOLD;
   final private static BigInteger TWO = new BigInteger("2");
   final private static ForkJoinPool fjp=new ForkJoinPool();
   private static long maxStealCount;
   private static int maxThreadCount;


   public static void main (String args[]){
     Faculteit f = new Faculteit(new BigInteger(args[0]));
     THRESHOLD = new BigInteger(args[1]);
     long startTijd = System.currentTimeMillis();
     System.out.println(fjp.invoke(f));
     System.out.println(System.currentTimeMillis()-startTijd);

     System.out.println("maxStealCount: " +maxStealCount);
     System.out.println("maxThreadCount: "+maxThreadCount);

   }

   Faculteit(BigInteger einde){
     start = BigInteger.ONE;
     this.einde = einde;
   }

   Faculteit(BigInteger start, BigInteger einde){
     this.start=start;
     this.einde=einde;
   }

   public BigInteger product(BigInteger start, BigInteger einde){
     int tc = fjp.getRunningThreadCount();
     long sc = fjp.getStealCount();
     if(tc>maxThreadCount)maxThreadCount=tc;
     if(sc>maxStealCount)maxStealCount=sc;
     BigInteger result = BigInteger.ONE;
     for(BigInteger x=start;x.compareTo(einde)<=0;x=x.add(BigInteger.ONE)){
        result=result.multiply(x);
     }
     return result;
   }

   public BigInteger compute(){
     if(einde.subtract(start).compareTo(THRESHOLD)<0){
       return product(start,einde);
     }
     else {
       BigInteger split = start.add((einde.subtract(start)).divide(TWO));
       Faculteit f1 = new Faculteit(start, split);
       Faculteit f2 = new Faculteit(split.add(BigInteger.ONE),einde);
       f1.fork();
       return f2.compute().multiply(f1.join());
     }
     
   }
   


}
