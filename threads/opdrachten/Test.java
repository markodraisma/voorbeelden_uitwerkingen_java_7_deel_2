import java.util.concurrent.*;



class Test {

   public static void main(String args[]){
      ForkJoinPool fjp = new ForkJoinPool();
      fjp.invoke(new Teller(Integer.parseInt(args[0]),
                            Integer.parseInt(args[1])));
      fjp.shutdown();
      try{
      fjp.awaitTermination(10,TimeUnit.SECONDS);
      } catch(InterruptedException ie){}
   }

}


class Teller extends RecursiveAction {

   private final int start;
   private final int eind;
   private final static int THRESHOLD = 10;

   Teller(int start, int eind){
      this.start=start;
      this.eind=eind;
      System.out.println("Teller tussen " + start + " en " + eind);
   }

   public void compute(){
     if(eind-start <= THRESHOLD){
       for(int i=start;i<=eind;i++){
         System.out.println(i);
       }
     }
     else {

       int midden = start + (eind-start)/2;
       Teller t1 = new Teller(start, midden);
       Teller t2 = new Teller(midden + 1, eind);
       invokeAll(t1, t2);
     }

   }

}
