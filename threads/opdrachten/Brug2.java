import java.awt.Color;
import java.awt.Graphics;
import java.util.concurrent.locks.*;

public class Brug2 implements Runnable {
  private boolean open = false;                       
  int positie, lengte;
  public final Lock lock = new ReentrantLock();
  public final Condition brugVrij = lock.newCondition();
  public final Condition brugDicht = lock.newCondition();
  private Thread thread;
  
  
  public Brug2(int positie, int lengte) {  // maak brug aan
    this.positie=positie;                 // met bepaalde positie en lengte
    this.lengte=lengte;
    lock.lock();
    brugVrij.signalAll();
    lock.unlock();
    thread= new Thread(this);
    thread.start();
  } 
  
  public void run(){
    while(true){
      openSluit();
      try{Thread.sleep(open?2000:3000);} // laat de brug langer gesloten dan open
      catch(InterruptedException ie){}
    }
  }
  
  public void openSluit() {              // open of sluit de brug
     try{
      lock.lock();
      if(!open){
      try{
      brugVrij.await();
      } catch (InterruptedException ie){}
      }
      open = !open;
      if(!open){
        brugDicht.signalAll();
      }
     } finally{
      lock.unlock();
     }
  }

  public boolean open() {
    return open;
  }
  
  public void paint(Graphics g) {        // teken een geopende of gesloten brug
    g.setColor(Color.red);
    if(open())
      g.drawLine(positie,100,positie,100-lengte);
    else                            
      g.drawLine(positie,100,positie+lengte,100);
  }
  
}
