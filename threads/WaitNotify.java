class WaitNotify implements Runnable {

   int teller = 0;

   WaitNotify(){
     Thread t = new Thread(this);
     t.start();
   }

   public static void main(String args[]){
     new WaitNotify();
   }
 
   public void run(){
      Thread t1 = new EenMaker();
      Thread t2 = new TweeMaker();
      Thread t3 = new DrieMaker();
      t1.start();
      t2.start();
      t3.start();
      try{
      t1.join();
      t2.join();
      t3.join();}catch(InterruptedException ie){}
      System.out.println(teller);
   }

   synchronized void maakEen(){
     try{ Thread.sleep(1000); } catch(InterruptedException ie) {}
     if(teller==0)
     System.out.println(teller++);
     this.notify();
   }
   
   synchronized void maakTwee(){
     try{ Thread.sleep(500); } catch(InterruptedException ie) {}
     while(teller!=1){
        try{ this.wait();} catch(InterruptedException ie){}
     }
     System.out.println(teller++);
   }

  synchronized void maakDrie() {

     try{ Thread.sleep(100); } catch(InterruptedException ie) {}
     while(teller!=2){
         try{ this.wait(100);} catch(InterruptedException ie){}
     }
     System.out.println(teller++);
  }



  class EenMaker extends Thread {
    public void run(){
      maakEen();
    }
  }

  class TweeMaker extends Thread {
    public void run() {
      maakTwee();
    }
  }

  class DrieMaker extends Thread {
    public void run() {
      maakDrie();
    }
  }
}


