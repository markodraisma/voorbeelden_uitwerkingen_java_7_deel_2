class ThreadTest {

   public static void main(String args[]){
      Runner r1 = new Runner("r1");
      Thread t1 = new Thread(r1, "t1");
      Thread t2 = new Thread(r1, "t2");
      Thread t3 = new Thread(new Runner("t3"));
      Thread t4 = new Thread(new Runner("t4"));

      t1.start();
      t2.start();
      t3.start();
      t4.start();

      try {
        t1.join();
        t2.join();
        t3.join();
        t4.join();
      } catch(InterruptedException ie){

      }
      System.out.println("threads zijn klaar");
   }

}

class Runner implements Runnable {

   private String naam;

   Runner(String naam){
     this.naam=naam;
   }

   @Override
   public void run(){
      System.out.println(naam + ": " + Thread.currentThread());
      try {
      Thread.sleep((long)(1000*Math.random()));
      } catch(InterruptedException ie){}
      System.out.println(naam + " is klaar. ");
   }

}
