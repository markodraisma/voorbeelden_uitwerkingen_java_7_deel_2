import java.util.*;

class Pannenkoek {}

class Pannenkoeken {

  private final Deque<Pannenkoek> stapel = new LinkedList<>();
  private final int maxAantal;

  Pannenkoeken(int maxAantal){
    this.maxAantal = maxAantal;
  }

  public void bak(){
    stapel.push(new Pannenkoek());
  }
  
  public Pannenkoek pak() {
    return stapel.pop();
  }

  public boolean op(){
    return stapel.size()==0;
  }

  public boolean klaar(){
    return stapel.size()==maxAantal;
  } 

}


class Feestje {
  static Pannenkoeken pannenkoeken = new Pannenkoeken(10);
  static boolean doorgaan=true;
  public static void main(String args[]){
    Bakker bakker = new Bakker();
    Kind henk = new Kind("Henk");
    Kind piet = new Kind("Piet");
    Kind iris = new Kind("Iris");

    bakker.start();
    henk.start();
    piet.start();
    iris.start();
    try{
      bakker.join();
      henk.join();
      piet.join();
      iris.join();
    } catch(InterruptedException ie){
      System.err.println(ie);
    }
    System.out.println("Feestje is afgelopen");

  }
 
  static class Bakker extends Thread {

    public void run(){
      // 5 ladingen bakken
      for(int i=0;i<5;i++){
        bakken();
      }
      doorgaan=false;
    }

    public void bakken(){
      synchronized(pannenkoeken){
        while(!pannenkoeken.op()){
          try{
            pannenkoeken.wait();
          }
          catch(InterruptedException ie){
            System.err.println(ie);
          }
        } 
        while(!pannenkoeken.klaar()){
          pannenkoeken.bak();
          try{Thread.sleep(50 + (long)(Math.random()*100));}
          catch(InterruptedException ie){ie.printStackTrace();}
        }
        System.out.println("Pannenkoeken zijn klaar!");
        pannenkoeken.notifyAll();
      }
    }
  }

  static class Kind extends Thread {

    private final String naam;

    Kind( String naam){
      this.naam=naam;
    }

    public void run(){
      while(doorgaan){
        eten();
      }
    }

    public void eten(){
      synchronized(pannenkoeken){
        while(pannenkoeken.op()){
          try{pannenkoeken.wait();}
          catch(InterruptedException ie){}
        }
        while(!pannenkoeken.op()){
          pannenkoeken.pak();
          System.out.println("pannenkoek gegeten door " + naam);
          pannenkoeken.notify();
            try{pannenkoeken.wait();}
            catch(InterruptedException ie){ie.printStackTrace();}
        }
        pannenkoeken.notify();
     }
    }
  }
}
