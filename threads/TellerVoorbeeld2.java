import java.util.*;

class TellerVoorbeeld2 {

  private static int teller = 0; // gedeelde bron

  synchronized void hoogOp(){  // this is de monitor
    teller++;
  }
  
  void lees() {
    synchronized(this) {  // this is de monitor
      System.out.println(teller);
    }
  }
  
  public static void main(String args[]){
  
    TellerVoorbeeld2 voorbeeld = new TellerVoorbeeld2();
    List<Thread> taken = new ArrayList<>();
    
    int x=0;
    
    while(x++<1000){
      taken.add(voorbeeld.new Ophoger());
      taken.add(voorbeeld.new Lezer());
    } 
    
    for(Thread t:taken){t.start();}
    
    for(Thread t:taken){
       try{t.join();}
       catch(InterruptedException ie){}}
    System.out.println(teller);
  }

  class Ophoger extends Thread { public void run(){ hoogOp(); } }
  class Lezer extends Thread { public void run(){ lees(); } } 

}
