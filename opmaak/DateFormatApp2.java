import java.text.*;
import java.util.Date;

class DateFormatApp2 {
  public static void main (String args[]) {
  DateFormat df = DateFormat.getInstance();
  SimpleDateFormat sf = (SimpleDateFormat)df;
  sf.applyPattern("dd-MM-yyyy");
  try {
      Date nu = df.parse(args[0]);
//sf.applyPattern("EEE '-dag in jaar: 'DDD ' -week in jaar: 'ww");
  sf.applyPattern("M");
   System.out.println(df.format(nu));
  sf.applyPattern("MM");
   System.out.println(df.format(nu));
  sf.applyPattern("MMM");
   System.out.println(df.format(nu));
  sf.applyPattern("MMMM");
   System.out.println(df.format(nu));

      System.out.println(nu);
    } catch (ParseException pe) {System.err.println(pe.getMessage());}
  }
}
