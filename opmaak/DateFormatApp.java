import java.util.*;
import java.text.*;
import static java.text.DateFormat.*;

class DateFormatApp {
  public static void main (String args[]) {
    Date nu = new Date();
    DateFormat test = getInstance();
    DateFormat dfDefault = getDateTimeInstance();
    DateFormat dfShort =   getTimeInstance(SHORT);
    DateFormat dfMedium =  getDateTimeInstance(MEDIUM,MEDIUM);
    DateFormat dfLong  =   getTimeInstance(LONG);
    DateFormat dfFull =    getTimeInstance(FULL);
    //dfDefault.getCalendar().setLenient(false);
    
    System.out.println(test.format(nu));
    System.out.println(dfDefault.format(nu));
    System.out.println(dfShort.format(nu));
    System.out.println(dfMedium.format(nu));
    System.out.println(dfLong.format(nu));
    System.out.println(dfFull.format(nu));
    try{
    System.out.println(dfDefault.parse(args[0]));
    } catch(ParseException pe){System.err.println(pe);}
  }
}
