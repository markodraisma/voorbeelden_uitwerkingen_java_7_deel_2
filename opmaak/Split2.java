class Split2 {
  public static void main (String args[]) {
    String[] woorden;
    String tekst=args[2];
    int limiet = Integer.parseInt(args[1]);
    String scheidingsteken=args[0];
    woorden = tekst.split(scheidingsteken,limiet);
    for (String woord:woorden) {
      System.out.println(woord);
    }
  }
}
