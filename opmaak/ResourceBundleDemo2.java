import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Demonstreert het gebruik van een ResourceBundle
 *
 */
public class ResourceBundleDemo2 {

  public static void main(String[] args) {
    ResourceBundle greeting = ResourceBundle.getBundle("Greeting");
    System.out.println(greeting.getString("morning_greeting"));
    Locale.setDefault(Locale.GERMAN);
    greeting = ResourceBundle.getBundle("Greeting", Locale.CHINESE);
    System.out.println(greeting.getString("morning_greeting"));
    Locale.setDefault(Locale.ENGLISH);
    greeting = ResourceBundle.getBundle("Greeting", Locale.CHINESE);
    System.out.println(greeting.getString("morning_greeting"));

  }

}
