import java.util.Date;
class Format {
  public static void main (String args[]) {
    System.out.format("De prijs van %s is %-+10.2f Euro", "een paar schoenen", (float)120);
    System.out.println();
    System.out.format("Het is vandaag %1$tD.", new Date());
  }
}
