import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.util.Locale;

class NumberFormatApp2 {
   
   public static void main (String args[]) {
     NumberFormat nf = NumberFormat.getInstance();
     //nf.setMaximumFractionDigits(nf.getCurrency().getDefaultFractionDigits());
     DecimalFormat df = (DecimalFormat)nf;
     df.applyPattern("   #,###.## %;  -#,###.## %");
     for(int i=0;i<10;i++) {
       System.out.println(nf.format(5000 - Math.random()*10000));
     }     
  }
}
