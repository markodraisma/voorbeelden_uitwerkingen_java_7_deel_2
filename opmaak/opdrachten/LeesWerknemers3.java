import java.util.*;
import java.io.*;
import java.nio.file.*;

public class LeesWerknemers3{
  public static void main(String args[]){
    if(args.length==1) {
      Locale.setDefault(new Locale(args[0]));
    }
    else if(args.length==2) {
      Locale.setDefault(new Locale(args[0],args[1]));
    }
    try{
      File file = Paths.get("j_werknemers.csv").toFile();
      ResourceBundle bundle = ResourceBundle.getBundle("werknemers");
      String persnr = bundle.getString("persnr");
      String naam = bundle.getString("naam");
      String functie = bundle.getString("functie");
      String salaris = bundle.getString("salaris");
      String kantnr = bundle.getString("kantnr");
      String line = "----------";
      Scanner scanner = new Scanner(file);
      System.out.printf("%-10s %-10s %-10s %10s %10s%n", persnr, naam, functie, salaris, kantnr);
      System.out.printf("%-10s %-10s %-10s %10s %10s%n", line,  line, line, line, line);
      while(scanner.hasNextLine()){
        Scanner regel = new Scanner(scanner.nextLine());
        regel.useDelimiter(",");
        System.out.format("%-10d %-10s %-10s %,10d %10d%n",
            regel.nextInt(), regel.next(), regel.next(), regel.nextInt(), regel.nextInt());
      }
    }catch(IOException ioe){ioe.printStackTrace();}

  }
}
