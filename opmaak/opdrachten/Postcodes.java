import java.io.*;
import java.util.*;
import java.text.*;
import java.util.regex.*;

public class Postcodes {

   public static void main(String args[]) throws Exception {
     Pattern pattern = Pattern.compile("^([1-9]\\d{3})\\s*([A-Za-z]{2})$");
     String replace = "$1 $2";
     File file = new File("postcodes.txt");
     Scanner rows = new Scanner(file);
     while(rows.hasNextLine()){
       String row = rows.nextLine();
       Matcher matcher = pattern.matcher(row);
       System.out.println(matcher.replaceAll(replace).toUpperCase());
     }
   }
}
