import java.io.*;

public class ReactieTijd {
  public static void main(String args[]){
    Console console = System.console();
    console.readLine("Hoe snel is uw reactietijd? Druk op Enter om te beginnen. ");
    try{Thread.sleep(1000+(int)(Math.random()*3000));}
    catch(InterruptedException ie){}
    long begintijd = System.nanoTime();
    console.readLine("%nDruk op Enter");
    long eindtijd = System.nanoTime();
    console.format("Reactietijd: %.3f seconden.", (eindtijd-begintijd)/1_000_000_000d);
  }
}
