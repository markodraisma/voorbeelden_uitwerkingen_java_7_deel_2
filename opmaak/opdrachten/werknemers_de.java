import java.util.*;

public class werknemers_de extends ListResourceBundle {

  public Object[][] getContents(){
    return new Object[][] { 
      {"naam", "name"},
      {"persnr", "persnr"},
      {"salaris","gehalt"},
      {"functie","funktion"},
      {"kantnr","büronr"}

    };
  }

}
