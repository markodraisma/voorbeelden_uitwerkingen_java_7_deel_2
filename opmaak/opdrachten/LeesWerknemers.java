import java.util.*;
import java.io.*;
import java.nio.file.*;

public class LeesWerknemers{
  public static void main(String args[]){
    try{
      File file = Paths.get("j_werknemers.csv").toFile();
      Scanner scanner = new Scanner(file);
      Locale nl = new Locale("nl");
      Locale.setDefault(nl);
      while(scanner.hasNextLine()){
        Scanner regel = new Scanner(scanner.nextLine());
        regel.useDelimiter(",");
        System.out.format("%-5d %-10s %-10s %,6d %2d%n",
            regel.nextInt(), regel.next(), regel.next(), regel.nextInt(), regel.nextInt());
      }
    }catch(IOException ioe){ioe.printStackTrace();}


   System.out.printf("%n%nboolean:%n");
   System.out.printf("%b %B %b %B%n",null,false,-1,2);
   System.out.printf("%n%ncharacter:%n");
   System.out.printf("%c %C %c %C", 'a', 'b', 100, 120);

  }
}
