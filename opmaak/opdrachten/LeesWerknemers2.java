import java.util.*;
import java.io.*;
import java.nio.file.*;

public class LeesWerknemers2{
  public static void main(String args[]){
    try{
      File file = Paths.get("j_werknemers2.csv").toFile();
      Scanner scanner = new Scanner(file);
      while(scanner.hasNextLine()){
        Scanner regel = new Scanner(scanner.nextLine());
        regel.useDelimiter(",");
        int persnr = regel.nextInt();
        String naam = regel.next();
        String functie = regel.next();
        String mgr = "";
        if(regel.hasNext()){
          mgr=regel.next();
        } else {
          regel.next();
        }
        int sal = regel.nextInt();
        int toeslag = 0;
        if(regel.hasNextInt()){
          toeslag=regel.nextInt();
        }
        else {
          regel.next();
        }
        int kantnr=regel.nextInt();
        System.out.format("%-5d %-10s %-10s %-5s %,6d %,6d %2d%n",
            persnr, naam, functie, mgr, sal, toeslag, kantnr);
      }
    }catch(IOException ioe){ioe.printStackTrace();}
  }
}
