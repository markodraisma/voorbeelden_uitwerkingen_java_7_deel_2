import java.util.Locale;

class LocaleApp2 {
  public static void main (String args[]) {
  for (Locale naam:Locale.getAvailableLocales()) 
    System.out.println(naam.getDisplayLanguage() + ": " + 
    naam.getDisplayCountry(Locale.ENGLISH));
  }
}

