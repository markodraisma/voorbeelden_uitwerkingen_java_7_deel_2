import java.util.ListResourceBundle;

public class Greeting_en extends ListResourceBundle {
  @Override
  protected Object[][] getContents() {
    return contents;
  }

  static final Object[][] contents = {
    { "morning_greeting", "Good morning from ListResourceBundle" },
    { "evening_greeting", "Good evening" },
    { "night_greeting", "Good night" } 
  };
}
